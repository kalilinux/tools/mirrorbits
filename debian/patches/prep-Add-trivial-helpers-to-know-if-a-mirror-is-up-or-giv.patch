From: Arnaud Rebillout <arnaudr@kali.org>
Date: Thu, 9 Jan 2025 21:56:21 +0700
Subject: prep: Add trivial helpers to know if a mirror is up or give its
 status

Those functions are trivial for now, but in the upcoming commits we will
allow a mirror to support both HTTP and HTTPS. The logic to know if it's
up will be more complex, given that a mirror can be up for HTTP
requests, but down for HTTPS requests.

Similarily, a status string will be a bit more thant just "up" or
"down", eg. when the mirror is up for HTTP and down for HTTPS.
---
 cli/commands.go    | 22 +++++++++++++++-------
 daemon/monitor.go  |  2 +-
 mirrors/mirrors.go |  5 +++++
 3 files changed, 21 insertions(+), 8 deletions(-)

diff --git a/cli/commands.go b/cli/commands.go
index 3654291..3067f08 100644
--- a/cli/commands.go
+++ b/cli/commands.go
@@ -202,7 +202,7 @@ func (c *cli) CmdList(args ...string) error {
 			}
 		}
 		if *down == true {
-			if mirror.Up == true || mirror.Enabled == false {
+			if IsUp(mirror) || mirror.Enabled == false {
 				continue
 			}
 		}
@@ -234,10 +234,8 @@ func (c *cli) CmdList(args ...string) error {
 		if *state == true {
 			if mirror.Enabled == false {
 				fmt.Fprintf(w, "\tdisabled")
-			} else if mirror.Up == true {
-				fmt.Fprintf(w, "\tup")
 			} else {
-				fmt.Fprintf(w, "\tdown")
+				fmt.Fprintf(w, "\t%s", StatusString(mirror))
 			}
 			fmt.Fprintf(w, " \t(%s)", stateSince.Format(time.RFC1123))
 		}
@@ -249,6 +247,18 @@ func (c *cli) CmdList(args ...string) error {
 	return nil
 }
 
+func IsUp(m *rpc.Mirror) bool {
+	return m.Up
+}
+
+func StatusString(m *rpc.Mirror) string {
+	if m.Up == true {
+		return "up"
+	} else {
+		return "down"
+	}
+}
+
 func (c *cli) CmdAdd(args ...string) error {
 	cmd := SubCmd("add", "[OPTIONS] IDENTIFIER", "Add a new mirror")
 	http := cmd.String("http", "", "HTTP base URL")
@@ -1014,10 +1024,8 @@ func (c *cli) CmdStats(args ...string) error {
 		fmt.Fprintf(w, "Identifier:\t%s\n", name)
 		if !reply.Mirror.Enabled {
 			fmt.Fprintf(w, "Status:\tdisabled\n")
-		} else if reply.Mirror.Up {
-			fmt.Fprintf(w, "Status:\tup\n")
 		} else {
-			fmt.Fprintf(w, "Status:\tdown\n")
+			fmt.Fprintf(w, "Status:\t%s\n", StatusString(reply.Mirror))
 		}
 		fmt.Fprintf(w, "Download requests:\t%d\n", reply.Requests)
 		fmt.Fprint(w, "Bytes transferred:\t")
diff --git a/daemon/monitor.go b/daemon/monitor.go
index 6cca02b..c998c38 100644
--- a/daemon/monitor.go
+++ b/daemon/monitor.go
@@ -461,7 +461,7 @@ func (m *monitor) syncLoop() {
 				goto end
 			}
 
-			if err == nil && mir.Enabled == true && mir.Up == false {
+			if err == nil && mir.Enabled == true && mir.IsUp() == false {
 				m.healthCheckChan <- id
 			}
 
diff --git a/mirrors/mirrors.go b/mirrors/mirrors.go
index e8c1d86..4c99a60 100644
--- a/mirrors/mirrors.go
+++ b/mirrors/mirrors.go
@@ -75,6 +75,11 @@ func (m *Mirror) IsHTTPS() bool {
 	return strings.HasPrefix(m.HttpURL, "https://")
 }
 
+// IsUp returns true if the mirror is up
+func (m *Mirror) IsUp() bool {
+	return m.Up
+}
+
 // Mirrors represents a slice of Mirror
 type Mirrors []Mirror
 
